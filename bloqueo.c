/*
El programa bloqueo.c recibe 3 argumentos:
    El primero es el nombre de un archivo.
    El segundo es una posición (offset) del archivo.
    El tercero es una candidad en bytes (nbytes)
El programa deberá bloquear el archivo indicado, a partir del offset (utilice lseek) y el tamaño del registro será nbytes.
Si se puede realizar el bloqueo, el programa esperará una entrada
del teclado y terminará. En caso de que no pueda bloquear el registro,
terminará enviando un mensaje indicando que no puede bloquear el
registro.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
 
#define EQ(str1,str2) (strcmp(str1,str2)==0)
//Funcion con bloqueo
int bloqueo(int fd, int orden, long longitud)
{
    struct flock candado;
    candado.l_type = orden;
    candado.l_whence = SEEK_CUR;
    candado.l_start = 0;
    candado.l_len = longitud;
  //Intenta bloquear hasta que le sea posible
    if (fcntl(fd,F_SETLK,&candado)==-1)
    {
    	printf("Error, el registro no puede ser bloqueado\n");
        exit(-1);
    }
    else if(orden==F_WRLCK){
    	printf("Presione enter");
    	getchar();
    }
}
int main(int argc, char *argv[]){
	int fd;
	if (argc!=4){ //revisa la cantidad de argumentos
		printf("Modo de uso:\n %s archivo offset nbytes\n", argv[0]);
		exit(0);
	}
	if((fd=open(argv[1],O_RDWR|O_CREAT,0644))==-1){
		perror(argv[1]);
		exit(-1);
	}
	lseek(fd,atol(argv[2]),SEEK_SET); 
	bloqueo(fd, F_WRLCK, atol(argv[3]));
	bloqueo(fd, F_UNLCK, atol(argv[3]));
	close(fd);
}