/*
 * sistema.c
 Programa que simula un interprete de comandos
 parte dos, incluye:
  Redireccionamientos |,<,>
  Segundo plano & NOTA: Separar el & con un espacio del comando
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

#define CARMAX 30

enum redireccion{redP,redD,redI};

int redIzquierda(char *com, char *archivo);
int redDerecha(char *com, char *archivo);
int pipes(char *comando1, char *comando2);
void ctrlC(int senal);
int sistema (const char *orden);
void cambioDir(char *entrada);
int BuscaCaracter (char *cadena, char caracter);
int separaCadenas(char *cadena, int bandera, int posicion);

int main (int argc, char *argv[])
{
  int estado, posicion;
  char *comando;
  comando=(char *)malloc(CARMAX);
  signal( SIGINT , ctrlC );
  while(1){
    printf("> ");
    scanf("%[^\n]", comando);
    getchar();
    posicion=BuscaCaracter(comando,'|');
    if (posicion>0)    separaCadenas(comando,redP,posicion);
    else{
      posicion=BuscaCaracter(comando,'>');
      if (posicion>0){
        separaCadenas(comando,redD,posicion);
      }
      else{
        posicion=BuscaCaracter(comando,'<');
          if (posicion>0){
            separaCadenas(comando,redI,posicion);
          }
          else sistema(comando);
      }
    }
  }
  return 0;
}

void ctrlC(int senal){ printf("\n");}

void cambioDir(char *entrada){
  int contador;
  char *comando[10], delimitador[]=" ";
  char *token = strtok(entrada, delimitador);
  if(token != NULL){
        while(token != NULL){
            // Sólo en la primera pasamos la orden; en las siguientes pasamos NULL
            comando[contador]=token;
            contador++;
            token = strtok(NULL, delimitador);
        }
    }
    comando[contador]=NULL;
    if (chdir(comando[1])) perror (comando[1]);
} 

int separaCadenas(char *cadena, int bandera, int posicion){
  char *cad1, *cad2;
  cad1=(char *)malloc(CARMAX);
  cad2=(char *)malloc(CARMAX);
  int i;
  for (i=0;i<posicion;i++) cad1[i]=cadena[i];
  cad1[posicion]='\0';
  i=0; posicion ++;
  while(cadena[i]!='\0') {cad2[i]=cadena[posicion+i]; i++;}
  cad2[posicion+i]='\0';
  if (bandera==redP) pipes(cad1, cad2);
  else if (bandera==redD) redDerecha(cad1,cad2);
  else if (bandera==redI) redIzquierda(cad1, cad2);
  else{
  printf("Bandera: %d\n", bandera);
  printf("c1: %s\n", cad1);
  printf("c2: %s\n", cad2);}
  free(cad1);
  free(cad2);
}
 
int BuscaCaracter (char *cadena, char caracter)
{
  int i=0;
  //Si encuentra el caracter regresa la posicion
  for(i=0;i<strlen(cadena);i++) if ( cadena[i] == caracter ) return i; 
  return -1; //Si no lo encuentra
}

int sistema (const char *orden)
{
  int pid, tty, estado, w, contador=0, segundoP;
  char *comando[30], delimitador[]=" ";
  char *token = strtok(orden, delimitador);
 
  fflush(stdout);   /* vacía el buffer de salida estandar */
 
  /* Apertura de la terminal asociada al aproceso */

  if(!strncmp(orden,"exit",4)) exit(0);
  else if(!strncmp(orden,"pwd",3)) {printf ("%s\n",get_current_dir_name()); return 0;}
  else if(!strncmp(orden,"cd",2)) {cambioDir(comando); return 0;}
 
  if ((tty=open ("/dev/tty",O_RDWR)) == -1)
  {
     perror ("sistema");
     return (-1);
  }
 
  if ((pid=fork())==0)
  {
     /* Código para el proceso hijo */
     /* Reasignamos los descriptores estándar de entrada, salida y errores
      * del proceso hijo para independizarlos de los del proceso padre */
 
     close (0); dup(tty);
     close (1); dup(tty);
     close (2); dup(tty);
     close (tty);
     //Revisa si el comando se ejecutara en background
     segundoP=BuscaCaracter(&orden, '&');
      printf("%d\n", segundoP);
     //Pasa de cadena a arreglo
    if(token != NULL){
        while(token != NULL){
            // Sólo en la primera pasamos la orden; en las siguientes pasamos NULL
            comando[contador]=token;
            contador++;
            token = strtok(NULL, delimitador);
        }
    }
    comando[contador]=NULL;
    if (segundoP>0) {
      if ((pid=fork())==-1) {
       perror ("Error al ejecutar fork");
       exit(1);
      } 
      else if (pid == 0) {
        if (setsid() == -1 ) // el hijo se convierte en un proceso en 2o plano
          perror ("No es posible enviarlo a background");
        else if (execvp(comando[0], &comando)<0)
          perror ("Falla en la ejecución del comando");
        exit (1);            // el hijo no debería estar aquí
      }
      wait(NULL);
    }
    else execvp (comando[0],comando);
     exit(127);/* Código de salida en caso de que no se pueda ejecutar execlp */
   }
   /* Código para el proceso padre */
 
   close (tty);
   while ((w=wait(&estado))!=pid && w!=-1);

   if(w==-1) estado = -1;
   return estado;
}

int pipes(char *comando1, char *comando2){
  int tuberia[2], contador=0;
  int pid;
  char *comando[30], delimitador[]=" ";
  char *token ;

  if (pipe(tuberia)==-1)
  {
     perror("tuberia");
     exit(-1);
  }
  /* Crea loe procesos padre e hijo */
  if ((pid=fork())==-1)
  {
     perror ("fork");
     exit(-1);
  }
  else if (pid==0)
  {
     /* Código del proceso hijo */
     close (0);
     dup   (tuberia[0]); 
     close(tuberia[0]);   
     close(tuberia[1]);
     contador=0;
    token = strtok(comando2, delimitador);
    if(token != NULL){
        while(token != NULL){
            // Sólo en la primera pasamos la orden; en las siguientes pasamos NULL
            comando[contador]=token;
            contador++;
            token = strtok(NULL, delimitador);
        }
    }
    comando[contador]=NULL;
     execvp (comando[0],comando);
     /* El programa que se ejecute usará como entrada estandar la tuberia */
  }
  else  {
     /* Codigo del proceso padre */
     close(1);            
     dup (tuberia[1]);   
     close (tuberia[0]);  
     close (tuberia[1]); 
     /* Ejecucion del programa1 */
     contador=0;
    token = strtok(comando1, delimitador);
    if(token != NULL){
        while(token != NULL){
            // Sólo en la primera pasamos la orden; en las siguientes pasamos NULL
            comando[contador]=token;
            contador++;
            token = strtok(NULL, delimitador);
        }
    }
    comando[contador]=NULL;
     execvp (comando[0],comando);
  }
}

int redDerecha(char *com, char *archivo){
  int fd, contador;
  char *comando[30], delimitador[]=" ";
  char *token ;
  if ( (fd = open (archivo,O_WRONLY|O_CREAT|O_TRUNC,0666))==-1 ) {
     perror (archivo);
     exit(0);
  }
  close (1); 
  dup(fd);  
   contador=0;
    token = strtok(com, delimitador);
    if(token != NULL){
        while(token != NULL){
            // Sólo en la primera pasamos la orden; en las siguientes pasamos NULL
            comando[contador]=token;
            contador++;
            token = strtok(NULL, delimitador);
        }
    }
    comando[contador]=NULL;
     execvp (comando[0],comando);
  return 0;
}

int redIzquierda(char *com, char *archivo){
  int contador, fd;
  char *comando[30], delimitador[]=" ";
  char *token= strtok(com, delimitador);
  if(token != NULL){
        while(token != NULL){
            comando[contador]=token;
            contador++;
            token = strtok(NULL, delimitador);
        }
    }
    comando[contador]=NULL;
  if ( (fd = open (archivo,O_RDONLY))==-1 ) {
     perror (archivo);
     exit(0);
  }
  close (0); /* Cierra la salida estándar */
  dup(fd);   /* Conecta el archivo con la salida estándar */

  /* Ejecucion del programa */
  execvp (comando[0],comando);
  exit(-1);
}

int background(char *com){
  pid_t pid;
  int contador;
  char *comando[30], delimitador[]=" ";
  char *token= strtok(com, delimitador);
  if(token != NULL){
        while(token != NULL){
            comando[contador]=token;
            contador++;
            token = strtok(NULL, delimitador);
        }
    }
    comando[contador]=NULL;
  if ((pid=fork())==-1) {
     perror ("Error al ejecutar fork");
     exit(1);
  } else if (pid == 0) {
     if (setsid() == -1 ) // el hijo se convierte en un proceso en 2o plano
        perror ("No es posible enviarlo a background");
     else if (execvp(comando[0], comando)<0)
       perror ("Falla en la ejecución del comando");
     exit (1);            // el hijo no debería estar aquí
  }
  wait(&pid);
  return (0); 
}
