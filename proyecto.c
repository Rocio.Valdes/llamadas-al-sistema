/*
Proyecto: CONTABILIDAD DEL SISTEMA DE ARCHIVOS
@Author: Valdes Vargas Rocio Monserrat
@Version: 1.0
@Date: 6 June 2019
*/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>

// Numero máximo de niveles de directorios
#define MAX_LEVEL 10
// Numero máximo de usuarios y grupos
#define MAX_REG 100
enum boolean {NO, SI};

struct opciones
{
	unsigned usuario:1;
	unsigned grupo:1;
};

//NOTA: crear unapara los nombres
unsigned usuarios[MAX_REG];
float espUsu[MAX_REG];
unsigned grupos[MAX_REG];
float espGru[MAX_REG];
unsigned numReU=0;
unsigned numReG=0;
char *usuLog[MAX_REG];
char *gruLog[MAX_REG];

//FUNCIONES
//Función de desglose de archivos debajo de la ruta especificada
int desglose (char *path, struct opciones opciones);
//Función que imprime portada
void presenta ();

int main(int argc, char *argv[])
{
	struct opciones opciones;
	/*
		found -> FLAG, si encuentra grupo o usuario
		ruta -> FLAG, si pasan rutas
	*/
	int i, j, found, ruta;
	char *bUsu, *bGru;
	presenta();
	//Para guardar los login
	for(i=0;i<MAX_REG;i++)
	{
		usuLog[i]=(char *)malloc(20);
		gruLog[i]=(char *)malloc(20);
	}
	//Revisar que opciones estan activas
	for (i=1;i<argc;i++)
		if (argv[i][0]=='-')
			//for (j=1;argv[i][j]!=0;j++)
			switch (argv[i][1])
			{
				case 'u':
					opciones.usuario=SI;
					bUsu=argv[i+1];
					//printf("Recuperando usuario: %s\n", bUsu);
					break;
				case 'g':
					opciones.grupo=SI;
					bGru=argv[i+1];
					//printf("Recuperando Grupo: %s\n", bGru);
					break;
				default: fprintf (stderr,"Opción [-%c] no definida\n",argv[i][j]);
					exit(-1);
			}
	//Analisis de cada directorio en los argumentos
	ruta=0;
	for (i=1;i<argc;i++)
		if (argv[i][0]!='-' && argv[i]!=bUsu && argv[i]!=bGru)
		{
			desglose(argv[i], opciones);
			ruta=1;
		}
	if(ruta!=1)
		desglose("./", opciones);
	if (opciones.usuario!=SI && opciones.grupo!=SI)
	{
		printf("-----   USUARIOS   -----\n");
		for(i=0;i<=numReU;i++)
			printf("U:%d\t:%s\t:%.3f\n", usuarios[i],usuLog[i],espUsu[i]);
		printf("-----   GRUPOS   -----\n");
		for(i=0;i<=numReG;i++)
			printf("G:%d\t:%s\t:%.3f\n", grupos[i],gruLog[i],espGru[i]);
	}
	else
	{
		if (opciones.usuario==SI)
		{
			found=0;
			for(i=0;i<=numReU;i++)
				if(usuarios[i]==atoi(bUsu))
				{
					printf("U:%d\t:%s\t:%.3f\n", usuarios[i],usuLog[i],espUsu[i]);
					found=1;
				}
			if(found!=1)
				printf("El usuario %s no tiene archivos en estos directorios\n", bUsu);
		}
		if (opciones.grupo==SI)
		{
			found=0;
			for(i=0;i<numReG;i++)
				if(grupos[i]==atoi(bGru))
				{
					printf("G:%d\t:%s\t:%.3f\n", grupos[i],gruLog[i],espGru[i]);
					found=1;
				}
			if (found!=1)
				printf("El grupo %s no tiene archivos en estos directorios\n", bGru);
		}
	}
	
}
int desglose (char *path, struct opciones opciones){
	DIR *dirp;
	struct dirent *dp; 
	static nivel=0;
	struct stat buf;
	struct passwd *pw;
	struct group  *gr;
	int ok,i,cont;
	char archivo[256],tipo_archivo;
	if (nivel == 0) printf("%s\n", path);
	if (nivel > MAX_LEVEL) return;

	//Se abre el directorio
	if ((dirp=opendir(path))==NULL)
	{
		perror(path);
		return;
	}
	/*Las dos primeras entradas son . (Directorio actual)
	y .. (Directorio padre), por lo que se comienza en el
	tercer entrada*/
	seekdir (dirp,2);
	//Se lee cada entrada del directorio
	while((dp=readdir(dirp))!=NULL)
	{
		//Para asegurarnos de no leer el directorio actual o el padre
		if (strcmp(dp->d_name,".")==0) continue;
		if (strcmp(dp->d_name,"..")==0) continue;
		
		//Para cada entrada se crea el path_name
		sprintf(archivo, "%s/%s", path, dp->d_name);

		//Lee el inodo del archivo
		ok=stat(archivo,&buf);

		//Si es un directorio se llama recursivamente
		if (ok!=-1 && (buf.st_mode & S_IFMT)==S_IFDIR)
		{
    		++nivel;
    		desglose(archivo,opciones);
    		--nivel;
		}

		//Si no es un directorio
		else if (ok!=-1)
		{
			//Asigna a cada uno de los usuarios
			if (numReU>0){
				cont=0;
				while(cont<=numReU && usuarios[cont]!=buf.st_uid)
					cont++;
				usuarios[cont]=buf.st_uid;
				espUsu[cont]=espUsu[cont]+(float)buf.st_size/1000/512;
				if ((pw=getpwuid(buf.st_uid))==NULL) strcpy(usuLog[cont], "?");
				else strcpy(usuLog[cont], pw->pw_name);
				if(numReU<cont) numReU=cont;
			}
			else{
				usuarios[0]=buf.st_uid;
				espUsu[0]=(float)buf.st_size/1000/512;
				if ((pw=getpwuid(buf.st_uid))==NULL) strcpy(usuLog[0], "?");
				else strcpy(usuLog[0], pw->pw_name);
				numReU++;
			}
			//Asigna a cada uno de los grupos
			if (numReG>0){
				cont=0;
				while(cont<=numReG && grupos[cont]!=buf.st_gid){
					cont++;
				}
				grupos[cont]=buf.st_gid;
				espGru[cont]=espGru[cont]+(float)buf.st_size/1000/512;
				if ((gr=getgrgid(buf.st_gid))==NULL) strcpy(gruLog[cont], "?");
				else strcpy(gruLog[cont], gr->gr_name);
				if(numReG<cont) numReG=cont;
			}
			else{
				grupos[0]=buf.st_gid;
				espGru[0]=(float)buf.st_size/1000/512;
				if ((gr=getgrgid(buf.st_gid))==NULL) strcpy(gruLog[0], "?");
				else strcpy(gruLog[0], gr->gr_name);
				numReG++;
			}

		}
	}
	closedir(dirp);
}
void presenta (){
	printf("*****************************************************************************\n");
	printf("                 Universidad Nacional Autónoma de México\n");
	printf(" Dirección General de Cómputo y de Tecnologías de Información y Comunicación\n");
	printf("                     COORDINACIÓN DE SUPERCÓMPUTO\n");
	printf("                          Llamadas al sistema\n");
	printf("****************************************************************************\n");
	printf("PROYECTO: Contabilidad del Sistema de Archivos\n");
	printf("Valdes Vargas Rocio Monserrat\n");
	printf("----------------------------------------------------------------------------\n");
}