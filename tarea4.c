/*
Programa que simula un ls con las opciones de -l y -R
*/

#include <stdio.h>
#include <time.h>
#include <pwd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <grp.h>

#define MAX_NIVEL 5
char permisos[]={'x','w','r'};
struct opciones
{
  unsigned recursivo;
  unsigned formato_largo;
};
enum boolean{NO,SI};
void infor (char *archivo);
int ls(char *path,struct opciones opcion);

int main(int argc, char *argv[]){
  struct opciones opciones;
  int i,j;
  char da;
  
  //REvisa si las opciones estan habilitadas
    for (i=1; i<argc; i++)
      if (argv[i][0]=='-')
         for (j=1; argv[i][j]!=0; j++)
             switch (argv[i][j])
             {
               case 'R': opciones.recursivo=SI; break;
               case 'l': opciones.formato_largo=SI; break;
               default : fprintf (stderr,"Opción [-%c] no definida\n",argv[i][j]);
                        exit(-1);
             }
      else if(argv[i]=="-lR" || argv[i]=="-Rl"){
        opciones.recursivo=SI;
        opciones.formato_largo=SI;
      }

  if(argc < 2 || (argc < 3 && argv[1][0]=='-'))
    ls(".",opciones);
  else{
    //REvisa los demas argumentos
    for (i=1;i<argc; i++)
       if (argv[i][0]!='-')
          ls(argv[i], opciones);
  }
  printf("\n");  
}

int ls(char *path, struct opciones opcion){
  DIR *dirp;
  struct dirent *dp;
  static nivel = 0;
  struct stat buf;
  int    ok,i, contDir;
  char   archivo[256], *arch;
  if (nivel==0)printf("\n%s\n",path);
  if(nivel>MAX_NIVEL) return;
  //Se abre el directorio
  if((dirp=opendir(path))==NULL){
    perror(path);
    return;
  }
  //Las dos primeras entradas son el directorio actual y el padre
  seekdir(dirp,2);
  //se lee cada una de las entradas del directorio
  while((dp=readdir(dirp))!=NULL){
    //Se crea el path_name correspondiente a la entrada leída
    if (strcmp(dp->d_name,".")==0) continue;
    if (strcmp(dp->d_name,"..")==0) continue;
    sprintf (archivo, "%s/%s", path, dp->d_name);
     /* Lectura del inodo del archivo */
    ok =stat(archivo,&buf);
 
    /* Si el archivo es un directorio, se llama nuevamente a tree recursivamente */
   if (ok!=-1 && (buf.st_mode & S_IFMT)==S_IFDIR && opcion.recursivo==SI && (dp->d_name)[0]!='.')
   {
     printf ("%s\n", dp->d_name);
     ++nivel;
     ls(archivo,opcion);
     --nivel;
     printf("\n");
   }
   /* Si el archivo no es un directorio */
   else if (ok!=-1)
   {
      arch=dp->d_name;
      if (arch[0]!='.'){
        if(opcion.formato_largo==SI) infor(arch); //si esta activo el formato largo
        else printf ("%s\t", dp->d_name);
      }
   }
  }
  closedir(dirp);
}

void infor (char *archivo)
{
  struct stat    buf;
  struct passwd *pw;
  struct group  *gr;
  int            i;
  char permi[9];
 
  if (stat(archivo, &buf)==-1)
  {
     perror(archivo);
     exit(-1);
  }
 
  //tipo:
  switch (buf.st_mode & S_IFMT)
  {
    case S_IFREG: printf ("o");   break;
    case S_IFDIR: printf ("d");   break;
    case S_IFCHR: printf ("c");   break;
    case S_IFBLK: printf ("b");   break;
    case S_IFIFO: printf ("p"); break;
    default: printf("?"); break;
  }

 
  /* Permisos */
  for (i=0; i<9; i++)
      if (buf.st_mode & (0400>>i)) permi[i]=permisos[(8-i)%3];
      else  permi[i]='-';
   //permisos especiales
  if (buf.st_mode & S_ISUID) {
    if(permi[5]=='-') permi[5]='s';
    else permi[5]='S';
  }
  if (buf.st_mode & S_ISGID){
    if(permi[2]=='-') permi[2]='s';
    else permi[2]='S';
  }
  if (buf.st_mode & S_ISVTX) {
    if(permi[8]=='-') permi[8]='t';
    else permi[8]='T'; 
  }
  for (i=0;i<9;i++) printf("%c", permi[i]);
 
  /* Ligas */
  printf (" %d",buf.st_nlink);
 
  /* UID y GID */
  if ((pw=getpwuid(buf.st_uid))==NULL) printf ("\t?");
  else printf (" %s", pw->pw_name);
  if ((gr=getgrgid(buf.st_gid))==NULL) printf ("\t?");
  else printf (" %s", gr->gr_name);
 
  /* Tamaño del archivo */
  printf ("\t%d", buf.st_size);
 
  /* Ultima modificación*/
  printf (" %s",asctime(localtime(&buf.st_mtime)));

  //nombre del archivo
  printf (" %s\n", archivo);
}
